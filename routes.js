var passport = require('passport');
var Account = require('./db/account');
var Event = require('./db/event');
var activeCities = require('./db/activeCities');
var mongoose = require('mongoose');
var db = mongoose.connection;
var startUpdate = true;

// Removes expired events; executed every minute
setInterval(function(){
    var currentTime = new Date();
    var $where = {end:{"$lt":new Date()}};
    Event.find( $where, function(err,docs){
        if (err) return console.log(err);
        if (!docs || !Array.isArray(docs) || docs.length === 0)
            return; //console.log('no docs found');
        docs.forEach( function (doc) {
            doc.remove();

        });
    });
},120000);


module.exports = function(app){

//  Routes index
    app.get('/', function(req, res) {
        res.render('index', {
            isAuthenticated: req.isAuthenticated(),
            user: req.user,
            username: req.username
        });
    });

// Route for main page
    app.get('/main', function(req,res){
        if (!req.isAuthenticated()) {
            res.redirect('/');
        } else {

            var starUpdate = false;

            //Finds out if more than 24 hours has passed so that the star flag gets set
            Account.findById(req.user._id).exec(function(err, user){
                // pseudo-code
                var lastUpdate = new Date(user.meta.stars);
                var now = new Date ();
                // get hours
                var hours = (now.getTime() - lastUpdate.getTime()) / 1000 / 60 / 60 ;
                if(hours>24){
                    starUpdate = true;
                    user.meta.stars = now;
                    user.save(function(err) {
                        if (err) {
                            console.log(err);
                        }
                    });
                }

                res.render('main', { user : req.user, flagBool:starUpdate});
            });
        }
    });


// Event page
    app.get('/addEvent', function(req, res) {
        if (!req.isAuthenticated()) {
            res.redirect('/');
        } else {
            res.render('addEvent',{user: req.user, failed:false});
        }

    });

// future spots page
    app.get('/futureSpots', function (req, res) {
        if (!req.isAuthenticated()) {
            res.redirect('/');
        } else {
            //      get data from database from city where user is at
            var query = Event.find({ 'address.city' : req.cookies.city, start:{"$gt":new Date((new Date().getTime()+ 86400000))} });

            query.sort({start:'asc'}).skip(req.query.count).limit(3);

            query.exec( function(err, result){
                if(err){
                    res.send(err);
                } else if(result.length ===  0 ){
                    res.render('futureSpots',{user : req.user, message: 'Sorry we did not find any future events in ' +  req.cookies.city , events:[]  });

                } else {
                    res.render('futureSpots', { user : req.user, message: false, events:result});
                }

            });
        }
    });

// Single Event Page
    app.get('/singleEvent',function(req,res){
        if(!req.isAuthenticated()) {
            res.redirect('/');
        } else {
            Event.find({'_id': req.query.id},function(err, event){
                if(err){
                    res.send(err);
                } else {
                    res.render('singleEvent', { user : req.user, events:event});
                }

            });
        }
    });

//  User authentication post
    app.post('/', passport.authenticate('local', {
        successRedirect: '/main',
        failureRedirect: '/'
    }));

//  Log out
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/')
    });

// Sign up page
    app.get('/signup',function(req, res){

        if( req.isAuthenticated()){
            res.redirect('/main', { user : req.user });
        } else{
            res.render('signup');
        }
    });

// Signup Post
    app.post('/signup', function(req,res){


        Account.register(new Account({
            username : req.body.username,
            contact: {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                address: req.body.addressLine1 + ' ' + req.body.addressLine2,
                city: req.body.city,
                state: req.body.state,
                zipcode: req.body.zipcode,
                country: req.body.country
            },
            friends:[],
            active_events:[],
            inactive_events:[],
            meta:{
                stars: new Date()
            }
        }), req.body.password, function(err, account) {

            if (err) {
                return res.render('signup', { account : account });
            }
            res.redirect('/');
        });
    });

// Api

// Post event to the database
    app.post('/event', function(req, res){
        if (!req.isAuthenticated()) {
            res.redirect('/');
        } else {

            Event.find({ 'address.lat' : req.body.lat, 'address.lon': req.body.lon  }, function(err, result){
                if(err){
                    res.render('addEvent',{user:req.user,failed:true,message:err});
                } else if(result.length ===  0 ){
                    //Store in database
                    var newEvent = new Event({
                        userId: req.user.id,
                        username:req.user.username.toString(),
                        title: req.body.title,
                        description: req.body.description,
                        webaddress: req.body.webaddress,
                        category: req.body.category,
                        start: new Date(req.body.date + ' ' + req.body.startTime),
                        end: new Date(req.body.date + ' ' + req.body.endTime),
                        address:{
                            street: (req.body.addressLine1 + ' ' + req.body.addressLine2),
                            city: req.body.city,
                            state: req.body.state,
                            zipcode: req.body.postal,
                            country: req.body.country
                        },
                        location:{
                            lat: req.body.lat,
                            lon: req.body.lon
                        },
                        votes: 0,
                        score: 0,
                        price: req.body.price,
                        private: req.body.private
                    });

                    newEvent.save(function(err) {
                        if (err){
                            console.log(err);
                            res.render('addEvent',{user:req.user,failed:true,message:err});
                        }
//                      add to activeCity if it does not exist
                        activeCities.find({'name':req.body.city}, function(err,city){
                            if(err){
                                console.log(err);
                            } else if (city.length === 0){
                                var newCity =  new activeCities({
                                    name:req.body.city,
                                    bounds:{
                                        lowLat: req.body.lowLat,
                                        lowLon: req.body.lowLon,
                                        highLat: req.body.highLat,
                                        highLon: req.body.highLon
                                    }
                                });

                                newCity.save(function(err){
                                    if(err){
                                        console.log(err);
                                    }
                                    res.redirect('/');
                                });
                            } else {
                                res.redirect('/');
                            }
                        });
                    });
                } else {
                    res.render('addEvent',{user:req.user,failed:true, message:"Event with this location exist!"});
                }
            });
        }
    });

// Gets a single event from database based on event id
    app.get('/events/:id', function(req, res){
        if(!req.isAuthenticated()){
            res.redirect('/');
        } else {
            // removes event my id
            Event.find({ '_id' :  req.params.id }, function(err, result){
                if(err){
                    res.send(err)
                } else  {
                    res.send(result);
                }

            });

        }

    });

// Gets events from the database based on the city and  less than currentDate field
    app.get('/events', function(req, res){

        if(!req.isAuthenticated()){
            res.redirect('/');
        } else {
            console.log("Searching: " + req.query.city.toString());
            // get data from database from city where user is at
            Event.find({ 'address.city' : req.query.town || req.query.city, start:{"$lt":req.query.currentDate} }, function(err, result){
                if(err){
                    res.send(err);
                } else  {
                    res.send(result);

                }

            });
        }

    });

//******Added for showing events as you zoom/scroll******
    app.get('/currentEvents', function(req, res){
        if(!req.isAuthenticated()){
            res.redirect('/');
        } else {
            // get data from database from city where user is at
            Event.find({start:{"$lt":req.query.currentDate} }, function(err, result){
                if(err){
                    res.send(err);
                } else  {
                    res.send(result);
                }

            });
        }

    });
//*******************************************************

    app.get('/activeCities',function(req, res){

        activeCities.find({},function(err, cities){
            res.send(cities);
        })
    });

    app.delete('/events/:id', function(req, res){
        if(!req.isAuthenticated()){
            res.redirect('/');
        } else {

            // removes event my id
            Event.findByIdAndRemove({ '_id' : req.params.id }, function(err){
                if(err){
                    res.send(err)
                } else  {
                    res.send({deleted:true});
                }

            });
        }

    });

    app.post('/comment',function(req,res){
        var newComment =  {
            content: req.body.userComment,
            user: req.user.username.toString(),
            timestamp: new Date()
        };

        Event.findByIdAndUpdate(req.body.id, {$push: {"comments": newComment}},
            function(err, event) {
                if(err){
                    console.log(error);
                } else {
                    res.end('{"success" : "Updated Successfully", "status" : 200}');
                }
            }
        );
    });

    app.post('/vote',function(req,res){

//      Find event by id and update the score(number of stars) and votes(number of user that voted)
        Event.findById(req.body.id).exec(function(err, event) {
            if (err) {
                return res.end('{"error" : "Failed to load event with id", "status" : 400}');
            }
            if (!event) {
                return res.end('{"error": "Failed to load event with id ' +  req.body.id +'", "status": 400}');
            }
            event.score += parseInt(req.body.userVote);



            if(req.body.update === 'false')
                event.votes++;

            event.save(function(err) {
                if (err) {
                    return res.end('{"error": "Cannot update the vote", "status": 500}');
                }
                return res.end('{"success" : "Updated Successfully","status": 200}');
            });
        });
    });

    app.get('/pullEvents',function(req,res){
        if (!req.isAuthenticated()) {
            res.redirect('/');
        } else {
            //      get data from database from city where user is at
            var query = Event.find({ 'address.city' : req.cookies.city, start:{"$gt":new Date((new Date().getTime()+ 86400000))}});

            query.sort({start:'asc'}).skip(parseInt(req.query.count)).limit(3);

            query.exec( function(err, result){
                if(err){
                    console.log(err);
                } else {
                    res.send(result);
                }

            });
        }
    });

    app.get('/currentHotness', function(req, res){
        console.log('voting for : '+ req.query.id);

        if(req.cookies.currentVote === req.query.id){

//          Deletes user current vote
            Event.findById(req.cookies.currentVote).exec(function(err, event) {

                if (err)
                    return res.send({message:'Sorry failed to delete user current vote',update:false});

                var userId = req.user.id;

                var id = mongoose.Types.ObjectId(userId);

                var userIndex = event.attendees.indexOf(id);

                if (userIndex > -1) {
                    event.attendees.splice(userIndex, 1);
                }

                event.save(function (err) {
                    if (err) {

                    }
                    console.log('deleting current vote');

                    return res.send({message: 'Your vote has been removed!', update: false});

                });
            });
        } else {
            if(req.cookies.currentVote !== 'Empty'){
                //clean up old vote if needed
                Event.findById(req.cookies.currentVote).exec(function(err, event){

                    if(err)
                        console.log(err);
                    if(!event){
                        // do nothing
                    } else{

                        var userId = req.user.id;

                        var id = mongoose.Types.ObjectId(userId);

                        var userIndex = event.attendees.indexOf(id);

                        if (userIndex > -1) {
                            event.attendees.splice(userIndex, 1);
                        }

                        event.save(function(err) {
                            if (err) {
                                return res.send({message:'Cannot update the vote',update:false});
                            }
                            console.log('cleaning up old vote');


                        });
                    }


                });

            }

            //      Find event by id and update the score(number of stars) and votes(number of user that voted)
            Event.findById(req.query.id).exec(function(err, event) {
                if (err) {
                    return res.send({message:'Failed to load event ',update:false});
                }
                if (!event) {
                    return res.send({message:'Failed to load event ',update:false});
                }

                var userId = req.user.id;

                var id = mongoose.Types.ObjectId(userId);

                event.attendees.push(id);

                event.save(function(err) {
                    if (err) {
                        return res.send({message:'Cannot update the vote',update:false});
                    }
                    console.log('new vote');

                    return res.send({message:'Hotness 1up!',update:true});
                });
            });
        }
    });



}
