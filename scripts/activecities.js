var request = require('request');
var fs = require('fs');
var split = require('split');
var forEachAsync = require('foreachasync').forEachAsync;

var cities = fs.readFileSync('cities.txt').toString().split('\n');

// mongodb client
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/hotSpot');

var activeCities = require('../db/activeCities');

var doc = [];

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongodb connection error:'));
db.once('open',fillActiveCities);

function fillActiveCities() {
    console.log('Mongodb Connected!');
    // waits for one request to finish before beginning the next
    forEachAsync(cities, function (next, city) {
        request('https://maps.googleapis.com/maps/api/geocode/json?address=' + city + ',California ,United States', function (error, response, body) {
            if (error) {
                console.log(error);
            } else {
                var city = JSON.parse(body);
                var newCity = new activeCities({
                    name: city.results[0].address_components[0].long_name,
                    bounds: {
                        lowLat: city.results[0].geometry.viewport.southwest.lat,
                        lowLon: city.results[0].geometry.viewport.southwest.lng,
                        highLat: city.results[0].geometry.viewport.northeast.lat,
                        highLon: city.results[0].geometry.viewport.northeast.lng
                    }
                });
                doc.push(newCity);
                console.log(city.results[0].address_components[0].long_name + ' done');
                next();
            }
        });
        // then after all of the elements have been handled
        // the final callback fires to let you know it's all done
    }).then(function () {
        for(var i = 0 ; i < doc.length; i++){
            doc[i].save();
        }
        console.log('All requests have finished');
    });

}


