var request = require('request');
var fs = require('fs');
var split = require('split');
var forEachAsync = require('foreachasync').forEachAsync;

var cities = fs.readFileSync('cities.txt').toString().split('\n');

// mongodb client
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/hotSpot');

var Event = require('../db/event');

var doc = [];
var id = mongoose.Types.ObjectId('546414649a7bfc175fc40a70');


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongodb connection error:'));
db.once('open',fill);

function fill() {
    console.log('Mongodb Connected!');
    // waits for one request to finish before beginning the next
    forEachAsync(cities, function (next, city) {
        console.log(city);
        request('http://api.bandsintown.com/events/search.json?location='+city+',California' +'&app_id=YOUR_APP_ID', function (error, response, body) {
            if (error) {
                console.log(error);
            } else {
                var data = JSON.parse(body);

                for(var i =0; i < data.length;i++){
                    var title = data[i].artists[0].name + " Live at " + data[i].venue.name;
                    var start = new Date(data[i].datetime);
                    start.setUTCHours(start.getUTCHours() + 8);
                    var end = new Date(data[i].datetime);
                    end.setHours(23);
                    end.setMinutes(59);
                    var street ='';
                    var city = data[i].venue.city;
                    var zipcode = '';
                    var state = 'California';
                    var lat = data[i].venue.latitude;
                    var lon = data[i].venue.longitude;
                    var webaddress =  data[i].url;
                    var description = 'Click title for more info....';
                    var randCategory = 'Music';


                    doc.push(new Event({
                        userId: id,
                        username: 'Bandsintown',
                        title: title,
                        description: description,
                        webaddress: webaddress,
                        category: randCategory,
                        start: start,
                        end: end,
                        address: {
                            street: street,
                            city: city,
                            state: state,
                            zipcode: zipcode,
                            country: 'United States'
                        },
                        location: {
                            lat: lat,
                            lon: lon
                        },
                        votes: 0,
                        score: 0,
                        price: 'Click Title for Details',
                        private: false
                    }));
                }

                next();
            }
        });
        // then after all of the elements have been handled
        // the final callback fires to let you know it's all done
    }).then(function () {
        for(var i = 0 ; i < doc.length; i++){
            doc[i].save();
        }
        console.log('All requests have finished');
    });

}



