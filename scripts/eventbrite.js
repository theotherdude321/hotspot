var request = require('request');
var fs = require('fs');
var split = require('split');


var categories = ['Other','Music','Comedy','Education','Food','Politics','Sports','Party'];
var cities = fs.readFileSync('cities.txt').toString().split('\n');

// mongodb client
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/hotSpot');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongodb connection error:'));
db.once('open',console.log.bind(console, 'Mongodb Connected!'));

var Event = require('../db/event');

var id = mongoose.Types.ObjectId('544fd135bfbae30254391796');

var doc = [];
var count = 0 ;



for(var i =0; i < cities.length; i++, count++) {
    request('https://www.eventbriteapi.com//v3/events/search?venue.city=' + cities[i] + '&token=KGNLXHH225537VTB5SBY', function (error, response, body) {
        if (error) {
            console.log(error);
        }
        var data = JSON.parse(body);
        for (var i = 0; i < data.events.length; i++) {

            var title = data.events[i].name.text;
            var start = new Date(data.events[i].start.utc);
            var end = new Date(data.events[i].end.utc);
            if (data.events[i].venue !== null) {
            var street = data.events[i].venue.address.address_1;
            var city = data.events[i].venue.address.city;
            var zipcode = data.events[i].venue.address.postal_code;
            var state = data.events[i].venue.address.region;
            var lat = data.events[i].venue.address.latitude;
            var lon = data.events[i].venue.address.longitude;
            }
            var webaddress = data.events[i].url;
            var description = data.events[i].description.text || ' ';
            var randCategory;

            if (data.events[i].category == null) {
                randCategory = categories[0];
            } else if (null||data.events[i].category.short_name == 'Music') {
                randCategory = categories[1];
            } else if (null||data.events[i].category.short_name == 'Arts') {
                randCategory = categories[0];
            } else if (null||data.events[i].category.short_name == 'Family & Education') {
                randCategory = categories[3];
            } else {
                randCategory = categories[0];

            }

            if(city === undefined){
            } else if (city === null) {
            } else {
                if( 86399106 >= (end - start)){
                    doc.push(new Event({
                        userId: id,
                        username: 'Eventbrite',
                        title: title,
                        description: description,
                        webaddress: webaddress,
                        category: randCategory,
                        start: start,
                        end: end,
                        address: {
                            street: street,
                            city: city,
                            state: state,
                            zipcode: zipcode,
                            country: 'United States'
                        },
                        location: {
                            lat: lat,
                            lon: lon
                        },
                        votes: 0,
                        score: 0,
                        price: 'Click Title for Details',
                        private: false
                    }));
                }

            }
        }

        for(var i = 0 ; i < doc.length; i++){
            doc[i].save();
        }
        console.log(city+' done');

    });
}