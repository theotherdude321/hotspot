//  Express Framwork
var express = require('express');

//  Passport Framework
var passport = require('passport');
var passportLocal = require('passport-local');

//  middle used by passport
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');

// mongodb client
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/hotSpot');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongodb connection error:'));
db.once('open',console.log.bind(console, 'Mongodb Connected!'));

// Express object
var app = express();

app.use(express.static(__dirname + '/public'));


//  View ejs
app.set('view engine', 'ejs');

//  Use body-parser middleware
app.use(bodyParser.urlencoded({
    extended: false
}));

//  Use cookie-parser middleware
app.use(cookieParser());

//  Use express-session middleware
app.use(expressSession({
    secret: process.env.SESSION_SECRET || 'secret',
    resave: false,
    saveUninitialized: false
}));

// Use Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Configure passport
var Account = require('./db/account');
passport.use(new passportLocal.Strategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());


// Routes
require('./routes')(app);

// Port
var port = process.env.PORT || 1337;

app.listen(port, function() {
    console.log('http://localhost:' + port + '/');
});
