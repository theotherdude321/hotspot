###must have installed###
- node
- bower
- nodemon

to install bower

 npm install -g bower
 npm install -g nodemon

###to get app running###

clone repository

 git clone https://bitbucket.org/eggytronix/hotspot.git

then run

  npm install
  cd public
  bower install
  cd ..
  nodemon server.js
