var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Event = new Schema({
    userId: Schema.Types.ObjectId,
    username:String,
    start: Date,
    end: Date,
    title: String,
    description: String,
    webaddress: String,
    category: String,
    address: {
        street: String,
        city: String,
        zipcode: String,
        state: String,
        country: String
    },
    location:{
        lat: Number,
        lon: Number
    },
    comments:[{
        content: String,
        user: String,
        timestamp:Date
    }],
    votes: Number,
    score: Number,
    price: String,
    private: {type: Boolean, default: false },
    attendees:[Schema.Types.ObjectID],
    gallery:[Schema.Types.ObjectID]
});


module.exports = mongoose.model('Event', Event);