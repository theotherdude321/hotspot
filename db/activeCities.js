var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var activeCity = new Schema({
    name:String,
    bounds:{
        lowLat: Number,
        lowLon: Number,
        highLat: Number,
        highLon: Number
    }
});

module.exports = mongoose.model('activeCity', activeCity);