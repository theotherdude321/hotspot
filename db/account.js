var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose');


var Account = new Schema({
    username: String,
    password: String,
    contact: {
        firstName: String,
        lastName: String,
        email: String,
        address: String,
        city: String,
        state: String,
        zipcode: Number,
        country: String
    },
    friends:[Schema.Types.ObjectId],
    active_events:[Schema.Types.ObjectId],
    inactive_events:[Schema.Types.ObjectId],
    meta:{
        stars:Date
    }

});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);